package com.bootcamp.memo.local

import androidx.room.*
import com.bootcamp.memo.model.TaskEntity

@Dao
interface TaskDao {

    @Insert
    fun insert(task : TaskEntity) : Long

    @Query("SELECT * FROM TaskEntity")
    fun getAll() : List<TaskEntity>

    @Update
    fun updateData(task : TaskEntity) : Int

    @Delete
    fun deleteData(taskEntity: TaskEntity) : Int
}