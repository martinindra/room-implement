package com.bootcamp.memo

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bootcamp.memo.local.TaskDatabase
import com.bootcamp.memo.model.TaskEntity
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.android.synthetic.main.activity_update.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UpdateActivity : AppCompatActivity() {

    val data : TaskEntity? by lazy {
        intent.getParcelableExtra<TaskEntity>("data")
    }

    private var taskDatabase : TaskDatabase? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)

        data?.let {
            edtxtTaskTitleUpdate.setText(it.title)
            edtxDescriptionUpdate.setText(it.description)
        }

        taskDatabase = TaskDatabase.getInstance(this)

        setSupportActionBar(toolbarUpdate)


        btn_show_main.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menuSave->{
                val titleUpdate = edtxtTaskTitleUpdate.text.toString()
                val descriptionUpdate = edtxDescriptionUpdate.text.toString()

                GlobalScope.launch {
                    val result = taskDatabase?.taskDao()?.updateData(TaskEntity(data?.id, titleUpdate, descriptionUpdate))

                    runOnUiThread {
                        if (result != 0){
                            edtxtTaskTitleUpdate.setText("")
                            edtxDescriptionUpdate.setText("")
                            Toast.makeText(this@UpdateActivity, "Update Berhasil untuk id ${data?.id}", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}