package com.bootcamp.memo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bootcamp.memo.adapter.TaskAdapter
import com.bootcamp.memo.local.TaskDatabase
import com.bootcamp.memo.model.TaskEntity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private var mDB : TaskDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDB = TaskDatabase.getInstance(this)
        fetchData()

        setSupportActionBar(toolbarMain)

        btn_add.setOnClickListener {
            val intent = Intent(this, TaskActivity::class.java)
            startActivity(intent)
        }
    }

    fun fetchData() {
        GlobalScope.launch {
            val listTask = mDB?.taskDao()?.getAll()

            runOnUiThread {
                listTask?.let {
                    val adapter = TaskAdapter(it, this@MainActivity)
                    rv_room.adapter = adapter
                    rv_room.setHasFixedSize(true)
                }
            }
        }
    }


}