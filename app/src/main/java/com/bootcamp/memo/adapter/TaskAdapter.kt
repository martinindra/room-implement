package com.bootcamp.memo.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bootcamp.memo.MainActivity
import com.bootcamp.memo.R
import com.bootcamp.memo.UpdateActivity
import com.bootcamp.memo.local.TaskDatabase
import com.bootcamp.memo.model.TaskEntity
import kotlinx.android.synthetic.main.list_room.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class TaskAdapter (val listMemo: List<TaskEntity>, var context: Context) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var title = itemView.title
        var description = itemView.description
        var delete = itemView.btn_delete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemview = LayoutInflater.from(parent.context).inflate(R.layout.list_room, parent, false)
        return ViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = listMemo[position].title.toString()
        holder.description.text = listMemo[position].description.toString()

        holder.itemView.setOnClickListener {
            val intent = Intent(context, UpdateActivity::class.java)
            intent.putExtra("data", listMemo[position])
            context.startActivity(intent)
        }

        holder.delete.setOnClickListener {
            AlertDialog.Builder(it.context).setPositiveButton("Ya"){
                    p0, p1 ->
                val mdb = TaskDatabase.getInstance(holder.itemView.context)

                GlobalScope.async {
                    val result = listMemo?.get(position)?.let { it1 ->
                        mdb?.taskDao()?.deleteData(
                            it1
                        )
                    }

                    (holder.itemView.context as MainActivity).runOnUiThread {
                        if (result != 0) {
                            Toast.makeText(
                                it.context,
                                "Data ${listMemo?.get(position)?.title} berhasil dihapus",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                it.context,
                                "Data ${listMemo?.get(position)?.title} gagal dihapus",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    (holder.itemView.context as MainActivity).fetchData()
                }
            }.setNegativeButton("Tidak"){
                    p0, p1 ->
                p0.dismiss()
            }.setMessage("Apakah anda yakin menghapus data tersebut").setTitle("Konfirmasi Hapus").create().show()
        }
    }

    override fun getItemCount(): Int = listMemo.size ?: 0
}